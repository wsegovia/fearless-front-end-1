import React, { useEffect, useState } from 'react';

function LocationForm(props) {
  const [states, setStates] = useState([]);
  const [name, setName] = useState('');
  const [roomCount, setRoomCount] = useState('');
  const [city, setCity] = useState('');
  const [state, setState] = useState('');

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setStates(data.states);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleRoomCountChange = (event) => {
    const value = event.target.value;
    setRoomCount(value);
  }

  const handleCityChange = (event) => {
    const value = event.target.value;
    setCity(value);
  }

  const handleStateChange = (event) => {
    const value = event.target.value;
    setState(value);  // Change to use setState
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    // create an empty JSON object
    const data = {};

    data.room_count = roomCount;
    data.name = name;
    data.city = city;
    data.state = state;  // Change to use state

    console.log(data);

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);

    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);
      setName('');
      setRoomCount('');
      setCity('');
      setState('');  // Change to use setState
    }
  }

  return (
    <form onSubmit={handleSubmit} id="create-location-form">
      <div>
        <p>A location form</p>

        <input
          onChange={handleNameChange}
          placeholder="Name" required
          type="text" name="name" id="name"
          className="form-control"
          value={name}
        />

        <input
          onChange={handleRoomCountChange}
          placeholder="Room Count" required
          type="number" name="room_count" id="room_count"
          className="form-control"
          value={roomCount}
        />

        <input
          onChange={handleCityChange}
          placeholder='City' required
          type="text" name="city" id="city"
          className="form-control"
          value={city}
        />

        <select required name="state" id="state" className="form-select" onChange={handleStateChange} value={state}>
          <option value="">Choose a state</option>
          {states.map(state => (
            <option key={state.abbreviation} value={state.abbreviation}>
              {state.name}
            </option>
          ))}
        </select>

        <button type="submit">Create</button>
      </div>
    </form>
  );
}

export default LocationForm;
