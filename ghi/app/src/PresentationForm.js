import React, { useEffect, useState } from 'react';

function PresentationForm() {
    const [conferences, setConferences] = useState([]);
    const [presenterName, setPresenterName] = useState('');
    const [presenterEmail, setPresenterEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [selectedConference, setSelectedConference] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handlePresenterNameChange = (event) => {
        const value = event.target.value;
        setPresenterName(value);
    }

    const handlePresenterEmailChange = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
    }

    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }

    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }

    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setSelectedConference(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            presenter_name: presenterName,
            presenter_email: presenterEmail,
            company_name: companyName,
            title: title,
            synopsis: synopsis,
            conference: selectedConference,
        };

        const presentationUrl = `http://localhost:8000/api/conferences/${selectedConference}/presentations/`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);

        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);
            setPresenterName('');
            setPresenterEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setSelectedConference('');
        }
    }

    return (
        <form onSubmit={handleSubmit} id="create-presentation-form">
            <div>
                <h1>Create a new presentation</h1>


                <input
                    onChange={handlePresenterNameChange}
                    placeholder="Presenter name" required
                    type="text" name="presenter_name" id="presenter_name"
                    className="form-control"
                    value={presenterName}
                />

                <input
                    onChange={handlePresenterEmailChange}
                    placeholder="Presenter email" required
                    type="email" name="presenter_email" id="presenter_email"
                    className="form-control"
                    value={presenterEmail}
                />

                <input
                    onChange={handleCompanyNameChange}
                    placeholder="Company name" required
                    type="text" name="company_name" id="company_name"
                    className="form-control"
                    value={companyName}
                />

                <input
                    onChange={handleTitleChange}
                    placeholder="Title" required
                    type="text" name="title" id="title"
                    className="form-control"
                    value={title}
                />

                <textarea
                    onChange={handleSynopsisChange}
                    placeholder='Synopsis' required
                    name="synopsis" id="synopsis"
                    className="form-control"
                    value={synopsis}
                />

                <select required name='conference' id="conference" className="form-select"
                    onChange={handleConferenceChange} value={selectedConference}>
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => (
                        <option key={conference.id} value={conference.id}>
                            {conference.name}
                        </option>
                    ))}
                    </select>

                <button type="submit">Create</button>
            </div>
        </form>
    );
}

export default PresentationForm;
