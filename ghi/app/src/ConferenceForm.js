import React, { useEffect, useState } from 'react';

function ConferenceForm(props) {
  const [locations, setLocations] = useState([]);
  const [name, setName] = useState('');
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [description, setDescription] = useState('');
  const [maxPresentations, setMaxPresentations] = useState('');
  const [maxAttendees, setMaxAttendees] = useState('');
  const [selectedLocation, setSelectedLocation] = useState('');

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleStartDateChange = (event) => {
    const value = event.target.value;
    setStartDate(value);
  }

  const handleEndDateChange = (event) => {
    const value = event.target.value;
    setEndDate(value);
  }

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  }

  const handleMaxPresentationsChange = (event) => {
    const value = event.target.value;
    setMaxPresentations(value);
  }

  const handleMaxAttendeesChange = (event) => {
    const value = event.target.value;
    setMaxAttendees(value);
  }

  const handleLocationChange = (event) => {
    const value = event.target.value;
    setSelectedLocation(value);

  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      name,
      starts: startDate,
      ends: endDate,
      description,
      max_presentations: maxPresentations,
      max_attendees: maxAttendees,
      location: selectedLocation,
    };

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(conferenceUrl, fetchConfig);

    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);
      setName('');
      setStartDate('');
      setEndDate('');
      setDescription('');
      setMaxPresentations('');
      setMaxAttendees('');
      setSelectedLocation('');
    }
  }

  return (
    <form onSubmit={handleSubmit} id="create-conference-form">
      <div>
        <p>A conference form</p>

        <input
          onChange={handleNameChange}
          placeholder="Name" required
          type="text" name="name" id="name"
          className="form-control"
          value={name}
        />

        <input
          onChange={handleStartDateChange}
          placeholder="Start Date" required
          type="date" name="start_date" id="start_date"
          className="form-control"
          value={startDate}
        />

        <input
          onChange={handleEndDateChange}
          placeholder="End Date" required
          type="date" name="end_date" id="end_date"
          className="form-control"
          value={endDate}
        />

        <textarea
          onChange={handleDescriptionChange}
          placeholder='Description' required
          name="description" id="description"
          className="form-control"
          value={description}
        />

        <input
          onChange={handleMaxPresentationsChange}
          placeholder="Maximum Presentations" required
          type="number" name="max_presentations" id="max_presentations"
          className="form-control"
          value={maxPresentations}
        />

        <input
          onChange={handleMaxAttendeesChange}
          placeholder="Maximum Attendees" required
          type="number" name="max_attendees" id="max_attendees"
          className="form-control"
          value={maxAttendees}
        />

        <select required name="location" id="location" className="form-select"
          onChange={handleLocationChange} value={selectedLocation}>
          <option value="">Choose a location</option>
          {locations.map(location => (
            <option key={location.id} value={location.id}>
              {location.name}
            </option>
          ))}
        </select>

        <button type="submit">Create</button>
      </div>
    </form>
  );
}

export default ConferenceForm;
